//
//  ViewController.swift
//  SdkTest
//
//  Created by Daniel Rein on 10/03/2016.
//  Copyright © 2016 pier39. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
      
        print("View did load")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

